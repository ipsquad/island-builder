/* This file is part of Island Builder

   Copyright (C) 2014-2017 Sean Harmer <sh@theharmers.co.uk>
   Copyright (C) 2017 Kevin Ottens <ervin@ipsquad.net>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License or any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

import Qt3D.Core 2.0

import IslandBuilder 1.0

Entity {
    id: root
    property alias heightMapSource: material.heightMapSource
    property alias xResolution: mesh.xResolution
    property alias zResolution: mesh.zResolution

    components: [
        TessellatedQuadMesh {
            id: mesh
            readonly property int heightMapSize: 1024
            readonly property int maxTessLevel: 64
            readonly property int resolution: material.maxTrianglesPerTexel * heightMapSize / maxTessLevel

            xResolution: resolution
            zResolution: resolution
        },
        TessellatedTerrainMaterial {
            id: material
            maxTrianglesPerTexel: 10
            xDivisions: mesh.xResolution
            zDivisions: mesh.zResolution
        }
    ]
}
