/* This file is part of Island Builder

   Copyright (C) 2014-2017 Sean Harmer <sh@theharmers.co.uk>
   Copyright (C) 2017 Kevin Ottens <ervin@ipsquad.net>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License or any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

import Qt3D.Render 2.0

Effect {
    id: root

    techniques: [
        Technique {
            filterKeys: FilterKey { name: "renderingStyle"; value: "forward" }

            graphicsApiFilter {
                api: GraphicsApiFilter.OpenGL
                majorVersion: 4
                minorVersion: 1
                profile: GraphicsApiFilter.CoreProfile
            }

            parameters: [
                Parameter { name: "light.position"; value: Qt.vector4d(0.5, 0.866, 0.0, 0.0) },
                Parameter { name: "light.intensity"; value: Qt.vector3d(1.0, 1.0, 1.0) },
                Parameter { name: "line.width"; value: 0.8 },
                Parameter { name: "line.color"; value: Qt.vector4d(1.0, 1.0, 1.0, 1.0) },
                Parameter { name: "viewportSize"; value: Qt.vector2d(_window.width, _window.height) }
            ]

            renderPasses: [
                RenderPass {
                    shaderProgram: ShaderProgram {
                        id: program
                        vertexShaderCode: loadSource("qrc:/shaders/terraintessellation.vert")
                        tessellationControlShaderCode: loadSource("qrc:/shaders/terraintessellation-cull-adaptive.tcs")
                        tessellationEvaluationShaderCode: loadSource("qrc:/shaders/terraintessellation.tes")
                        geometryShaderCode: loadSource("qrc:/shaders/terraintessellation.geom")
                        fragmentShaderCode: loadSource("qrc:/shaders/terraintessellation.frag")
                    }
                }
            ]
        }
    ]
}
