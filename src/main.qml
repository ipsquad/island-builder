/* This file is part of Island Builder

   Copyright (C) 2014-2017 Sean Harmer <sh@theharmers.co.uk>
   Copyright (C) 2017 Kevin Ottens <ervin@ipsquad.net>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License or any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

import Qt3D.Core 2.0
import Qt3D.Input 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0

Entity {
    id: root
    objectName: "root"

    // Use the renderer configuration specified in ForwardRenderer.qml
    // and render from the mainCamera
    components: [
        RenderSettings {
            ForwardRenderer {
                camera: mainCamera
                frustumCulling: false
                clearColor: Qt.rgba(0.65, 0.77, 1.0, 1.0)
            }
        },
        // Event Source will be set by the Qt3DQuickWindow
        InputSettings { }
    ]

    Camera {
        id: mainCamera
        position: Qt.vector3d(250.0, 10.0, 250.0)
        viewCenter: Qt.vector3d(250.0, 10.0, 249.0);
    }

    FirstPersonCameraController {
        camera: mainCamera
    }

    TessellatedHeightMap {
        heightMapSource: "assets/heightmap-1024x1024.png"
    }
}
