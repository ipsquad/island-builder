/* This file is part of Island Builder

   Copyright (C) 2014-2017 Sean Harmer <sh@theharmers.co.uk>
   Copyright (C) 2017 Kevin Ottens <ervin@ipsquad.net>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License or any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

#include <QGuiApplication>
#include <QQmlEngine>
#include <QQmlContext>
#include <Qt3DQuick/QQmlAspectEngine>
#include <Qt3DQuickExtras/Qt3DQuickWindow>

#include "tessellatedquadmesh.h"

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);
    Qt3DExtras::Quick::Qt3DQuickWindow view;

    qmlRegisterType<TessellatedQuadMesh>("IslandBuilder", 1, 0, "TessellatedQuadMesh");

    // Expose the window as a context property so we can set the aspect ratio
    auto rootContext = view.engine()->qmlEngine()->rootContext();
    rootContext->setContextProperty("_window", &view);
    rootContext->setContextProperty(QStringLiteral("ASSETS_PATH"),
                                    QUrl::fromLocalFile(QStringLiteral(ASSETS)));

    view.resize(800, 600);
    view.setSource(QUrl("qrc:/main.qml"));
    view.show();
    return app.exec();
}
