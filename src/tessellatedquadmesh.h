/* This file is part of Island Builder

   Copyright (C) 2014-2017 Sean Harmer <sh@theharmers.co.uk>
   Copyright (C) 2017 Kevin Ottens <ervin@ipsquad.net>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License or any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

#ifndef TESSELLATEDQUADMESH_H
#define TESSELLATEDQUADMESH_H

#include <Qt3DRender/QGeometryRenderer>

class TessellatedQuadMesh : public Qt3DRender::QGeometryRenderer
{
    Q_OBJECT
    Q_PROPERTY(int xResolution READ xResolution WRITE setXResolution NOTIFY xResolutionChanged)
    Q_PROPERTY(int zResolution READ zResolution WRITE setZResolution NOTIFY zResolutionChanged)

public:
    explicit TessellatedQuadMesh(Qt3DCore::QNode *parent = nullptr);

    void setXResolution(int xResolution);
    int xResolution() const;

    void setZResolution(int zResolution);
    int zResolution() const;

signals:
    void xResolutionChanged();
    void zResolutionChanged();

private:
    void installFactory();

    int m_xResolution;
    int m_zResolution;
};

#endif // TESSELLATEDQUADMESH_H
