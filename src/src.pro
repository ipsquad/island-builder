TEMPLATE = app
TARGET = island-builder

QT += 3dquickextras

DEFINES += ASSETS=\\\"$$PWD/assets\\\"

HEADERS += \
    tessellatedquadmesh.h

SOURCES += \
    main.cpp \
    tessellatedquadmesh.cpp

OTHER_FILES += \
    main.qml \
    TessellatedTerrainEffect.qml \
    TessellatedTerrainMaterial.qml \
    TessellatedHeightMap.qml \
    shaders/*

RESOURCES += \
    main.qrc
