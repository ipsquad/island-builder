/* This file is part of Island Builder

   Copyright (C) 2014-2017 Sean Harmer <sh@theharmers.co.uk>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License or any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

#version 400 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in worldVertex {
    vec4 worldPosition;
    vec3 worldNormal;
    vec4 position;
    vec3 normal;
    vec2 texCoords;
} gs_in[];

out WireFrameVertex {
    noperspective vec3 edgeDistance;
    vec4 worldPosition;
    vec3 worldNormal;
    vec4 position;
    vec3 normal;
    vec2 texCoords;
} gs_out;

uniform mat4 viewportMatrix;

void main()
{
    // Transform each vertex into viewport space
    vec2 p0 = vec2(viewportMatrix * (gl_in[0].gl_Position / gl_in[0].gl_Position.w));
    vec2 p1 = vec2(viewportMatrix * (gl_in[1].gl_Position / gl_in[1].gl_Position.w));
    vec2 p2 = vec2(viewportMatrix * (gl_in[2].gl_Position / gl_in[2].gl_Position.w));

    // Calculate lengths of 3 edges of triangle
    float a = length(p1 - p2);
    float b = length(p2 - p0);
    float c = length(p1 - p0);

    // Calculate internal angles using the cosine rule
    float alpha = acos((b * b + c * c - a * a) / (2.0 * b * c));
    float beta = acos((a * a + c * c - b * b) / (2.0 * a * c));

    // Calculate the perpendicular distance of each vertex from the opposing edge
    float ha = abs(c * sin(beta));
    float hb = abs(c * sin(alpha));
    float hc = abs(b * sin(alpha));

    // Now add this perpendicular distance as a per-vertex property in addition to
    // the position calculated in the vertex shader.

    // Vertex 0 (a)
    gs_out.edgeDistance = vec3(ha, 0, 0);
    gs_out.worldPosition = gs_in[0].worldPosition;
    gs_out.worldNormal = gs_in[0].worldNormal;
    gs_out.position = gs_in[0].position;
    gs_out.normal = gs_in[0].normal;
    gs_out.texCoords = gs_in[0].texCoords;
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    // Vertex 1 (b)
    gs_out.edgeDistance = vec3(0, hb, 0);
    gs_out.worldPosition = gs_in[1].worldPosition;
    gs_out.worldNormal = gs_in[1].worldNormal;
    gs_out.position = gs_in[1].position;
    gs_out.normal = gs_in[1].normal;
    gs_out.texCoords = gs_in[1].texCoords;
    gl_Position = gl_in[1].gl_Position;
    EmitVertex();

    // Vertex 2 (c)
    gs_out.edgeDistance = vec3(0, 0, hc);
    gs_out.worldPosition = gs_in[2].worldPosition;
    gs_out.worldNormal = gs_in[2].worldNormal;
    gs_out.position = gs_in[2].position;
    gs_out.normal = gs_in[2].normal;
    gs_out.texCoords = gs_in[2].texCoords;
    gl_Position = gl_in[2].gl_Position;
    EmitVertex();

    // Finish the primitive off
    EndPrimitive();
}
