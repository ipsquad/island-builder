/* This file is part of Island Builder

   Copyright (C) 2014-2017 Sean Harmer <sh@theharmers.co.uk>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License or any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

#version 400 core

in vec2 vertexPosition;

out terrainVertex {
    vec2 position;
    vec2 texCoord;
} vs_out;

uniform int xDivisions; // Number of patches in x dimension
uniform int zDivisions; // Number of patches in x dimension
uniform float horizontalScale;

void main()
{
    int i = gl_VertexID;
    int z = i / xDivisions;
    int x = i % xDivisions;
    vec2 delta = vec2(1.0 / xDivisions, 1.0 / zDivisions);
    vec2 tcDelta = vec2(1.0 / xDivisions, 1.0 / zDivisions);

    vs_out.position = vertexPosition;
    vs_out.texCoord = vec2(x * tcDelta.x, z * tcDelta.y);
}
