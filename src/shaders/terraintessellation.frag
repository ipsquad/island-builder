/* This file is part of Island Builder

   Copyright (C) 2014-2017 Sean Harmer <sh@theharmers.co.uk>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License or any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

#version 400 core

uniform struct FogInfo {
    vec4 color;
    float minDistance;
    float maxDistance;
} fog;

uniform struct LightInfo {
    vec4 position;  // Light position in eye coords.
    vec3 intensity;
} light;

uniform struct MaterialInfo {
    vec3 Ka;            // Ambient reflectivity
    vec3 Kd;            // Diffuse reflectivity
    vec3 Ks;            // Specular reflectivity
    float shininess;    // Specular shininess exponent
} material;

uniform sampler2D grassTexture;
uniform sampler2D rockTexture;
uniform sampler2D snowTexture;

in WireFrameVertex {
    noperspective vec3 edgeDistance;
    vec4 worldPosition;
    vec3 worldNormal;
    vec4 position;
    vec3 normal;
    vec2 texCoords;
} fs_in;

out vec4 fragColor;

void phongModel(out vec3 ambientAndDiff, out vec3 spec)
{
    // Some useful vectors
    vec3 s = normalize(vec3(light.position));
    vec3 v = normalize(-fs_in.position.xyz);
    vec3 n = normalize(fs_in.normal);
    vec3 r = reflect(-s, n);

    // Calculate the ambient contribution
    vec3 ambient = light.intensity * material.Ka;

    // Calculate the diffuse contribution
    float sDotN = max(dot(s, n), 0.0);
    vec3 diffuse = light.intensity * material.Kd * sDotN;

    // Sum the ambient and diffuse contributions
    ambientAndDiff = ambient + diffuse;

    // Calculate the specular highlight component
    spec = vec3(0.0);
    if (sDotN > 0.0) {
        spec = light.intensity * material.Ks *
               pow(max(dot(r, v), 0.0), material.shininess);
    }
}

float textureDistanceBlendFactor()
{
    float dist = abs(fs_in.position.z);
    return (dist - 30.0) / (30.0 - 5.0);
}

void nearAndFarTexCoords(out vec2 uvNear, out vec2 uvFar)
{
    uvNear = fs_in.texCoords * 100.0;
    uvFar = fs_in.texCoords * 10.0;
}

vec4 shadeLightingFactors()
{
    vec3 ambientAndDiff, spec;
    phongModel(ambientAndDiff, spec);
    vec4 lightingFactors = vec4(ambientAndDiff, 1.0) + vec4(spec, 1.0);
    return lightingFactors;
}

vec4 shadeTexturedAndLit()
{
    vec2 uvNear, uvFar;
    nearAndFarTexCoords(uvNear, uvFar);
    float textureDistanceFactor = textureDistanceBlendFactor();

    // Get grass texture color
    vec4 grassNear = texture(grassTexture, uvNear);
    vec4 grassFar = texture(grassTexture, fs_in.texCoords);
    vec4 grassColor = mix(grassNear, grassFar, textureDistanceFactor);

    // Get rock texture color
    vec4 rockNear = texture(rockTexture, uvNear);
    vec4 rockFar = texture(rockTexture, uvFar);
    vec4 rockColor = mix(rockNear, rockFar, textureDistanceFactor);

    // Blend rock and grass texture based upon the worldNormal vector
    vec4 grassRockColor = mix(rockColor, grassColor, smoothstep(0.75, 0.95, clamp(fs_in.worldNormal.y, 0.0, 1.0)));

    // Now blend with snow based upon world height
    vec4 snowNear = texture(snowTexture, uvNear);
    vec4 snowFar = texture(snowTexture, 5.0 * uvFar);
    vec4 snowColor = mix(snowNear, snowFar, textureDistanceFactor);

    vec4 diffuseColor = mix(grassRockColor, snowColor, smoothstep(10.0, 15.0, fs_in.worldPosition.y));

    // Calculate the lighting model, keeping the specular component separate
    vec3 ambientAndDiff, spec;
    phongModel(ambientAndDiff, spec);
    vec4 color = vec4(ambientAndDiff, 1.0) * diffuseColor + vec4(spec, 1.0);
    return color;
}

vec4 simpleFog(const in vec4 c)
{
    float dist = length(fs_in.position);
    float fogFactor = (fog.maxDistance - dist) / (fog.maxDistance - fog.minDistance);
    fogFactor = clamp(fogFactor, 0.0, 1.0);
    return mix(fog.color, c, fogFactor);
}

void main()
{
    // Compute fragment color depending upon selected shading mode
    //vec4 c = shadeLightingFactors();
    vec4 c = shadeTexturedAndLit();

    // Blend with fog color
    //fragColor = c;
    fragColor = simpleFog( c );
}
