/* This file is part of Island Builder

   Copyright (C) 2014-2017 Sean Harmer <sh@theharmers.co.uk>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License or any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

#version 400 core

// Only one vertex per patch (lower-left corner)
layout(vertices = 1) out;

// xz position from vertex shader
in terrainVertex {
    vec2 position;
    vec2 texCoord;
} tcs_in[];

// xz position to tessellation evaluation shader
out terrainVertex {
    vec2 position;
    vec2 texCoord;
} tcs_out[];

// Patch qualifier on interface blocks requires GLSL 420
patch out vec2 worldPatchExtent;
patch out vec2 texCoordPatchExtent;

uniform sampler2D heightMap;

// Fully tessellated!
const float tessellationLevel = 64.0;

// The number of triangles created per height-map texel
uniform int maxTrianglesPerTexel = 10;

// Distance between each tessellation point at max tess level
uniform float horizontalScale = 10.0;

// Vertical scale to multiply height samples by
uniform float verticalScale = 0.4;

// Transformation matrix
uniform mat4 mvp;

uniform float pixelsPerTriangleEdge = 12.0;

uniform vec2 viewportSize;

const float maxTessLevel = 64.0;

bool isOBBInViewFrustum(const in vec4 clipPositions[8])
{
    for (int plane = 0; plane < 3; ++plane) {
        // Test against {left, bottom, near} plane
        bool inside = false;
        for (int i = 0; i < 8; ++i) {
            if (clipPositions[i][plane] > -clipPositions[i][3]) {
                inside = true;
                break;
            }
        }

        if (!inside)
            return false;

        // Test against {right, top, far} plane
        inside = false;
        for (int i = 0; i < 8; ++i) {
            if (clipPositions[i][plane] < clipPositions[i][3]) {
                inside = true;
                break;
            }
        }

        if (!inside)
            return false;
    }

    return true;
}

// Calculate a fractional tessellation level from the clip-space positions
// of 2 vertices making up one edge of the patch, v1 and v2. The function
// works by constructing a sphere that encapsulates the edge and projecting
// this into screen space. We then use a target number of pixels per
// triangle edge to calculate the required tessellation level.
float calcTessellationLevel(const in vec4 v1, const in vec4 v2)
{
    vec4 p1 = 0.5 * (v1 + v2);
    vec4 p2 = p1;
    p2.y += distance(v1, v2);
    p1 = p1 / p1.w;
    p2 = p2 / p2.w;
    float l = length(0.5 * viewportSize * (p1.xy - p2.xy));
    float tessLevel = clamp(l / pixelsPerTriangleEdge, 1.0, 64.0);
    return clamp(pow(2.0, ceil(log2(tessLevel))), 0, maxTessLevel);
}

void main()
{
    // Pass along the vertex position unmodified
    tcs_out[gl_InvocationID].position = tcs_in[gl_InvocationID].position;
    tcs_out[gl_InvocationID].texCoord = tcs_in[gl_InvocationID].texCoord;

    // Calculate extent of this patch in texture coords [0,1]
    texCoordPatchExtent = maxTessLevel / (textureSize(heightMap, 0) * maxTrianglesPerTexel);
    worldPatchExtent = horizontalScale * texCoordPatchExtent;

    vec2 patchCornersXZ[4];
    patchCornersXZ[0] = tcs_out[gl_InvocationID].texCoord;                                    // min x, min z
    patchCornersXZ[1] = tcs_out[gl_InvocationID].texCoord + vec2(0.0, texCoordPatchExtent.y); // min x, max z
    patchCornersXZ[2] = tcs_out[gl_InvocationID].texCoord + vec2(texCoordPatchExtent.x, 0.0); // max x, min z
    patchCornersXZ[3] = tcs_out[gl_InvocationID].texCoord + texCoordPatchExtent;              // max x, max z

    // Conservative bounding cube is made up from four vertices at y = 0, and four
    // more vertices at y = verticalScale (since the texture lookup for height is in
    // the range [0,1]).
    vec4 clipSpacePatchCorners[8];
    for (int i = 0; i < 4; ++i) {
        vec4 position;
        position.xz = patchCornersXZ[i] * horizontalScale;
        position.y = verticalScale;
        position.w = 1.0;

        vec4 positionBase;
        positionBase.xz = position.xz;
        positionBase.y = 0.0;
        positionBase.w = 1.0;

        // Transform to clip-space
        clipSpacePatchCorners[2 * i] = mvp * position;
        clipSpacePatchCorners[2 * i + 1] = mvp * positionBase;
    }

    // Determine if we can cull this patch
    if (isOBBInViewFrustum(clipSpacePatchCorners)) {
        gl_TessLevelOuter[0] = calcTessellationLevel(clipSpacePatchCorners[0], clipSpacePatchCorners[2]); // x = min,   z = const
        gl_TessLevelOuter[1] = calcTessellationLevel(clipSpacePatchCorners[0], clipSpacePatchCorners[4]); // x = const, z = min
        gl_TessLevelOuter[2] = calcTessellationLevel(clipSpacePatchCorners[4], clipSpacePatchCorners[6]); // x = max,   z = const
        gl_TessLevelOuter[3] = calcTessellationLevel(clipSpacePatchCorners[2], clipSpacePatchCorners[6]); // x = const, z = max
        gl_TessLevelInner[0] = max(gl_TessLevelOuter[1], gl_TessLevelOuter[3]); // z = const
        gl_TessLevelInner[1] = max(gl_TessLevelOuter[0], gl_TessLevelOuter[2]); // x = const
    } else {
        // Set the tessellation levels to 0 to cull the patch
        gl_TessLevelOuter[0] = 0.0;
        gl_TessLevelOuter[1] = 0.0;
        gl_TessLevelOuter[2] = 0.0;
        gl_TessLevelOuter[3] = 0.0;
        gl_TessLevelInner[0] = 0.0;
        gl_TessLevelInner[1] = 0.0;
    }
}
