/* This file is part of Island Builder

   Copyright (C) 2014-2017 Sean Harmer <sh@theharmers.co.uk>
   Copyright (C) 2017 Kevin Ottens <ervin@ipsquad.net>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License or any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

import Qt3D.Render 2.0

Material {
    id: root

    property color ambient: Qt.rgba(0.1, 0.1, 0.1, 1.0)
    property color diffuse: Qt.rgba(1.0, 1.0, 1.0, 1.0)
    property color specular: Qt.rgba(0.3, 0.3, 0.3, 1.0)
    property real shininess: 10.0
    property real horizontalScale: 500.0
    property real verticalScale: 20.0
    property color fogColor: Qt.rgba(0.65, 0.77, 1.0, 1.0)
    property real minFogDistance: 50.0
    property real maxFogDistance: 128.0
    property string heightMapSource: "assets/heightmap-1024x1024.png"
    property string grassTextureSource: ASSETS_PATH + "/textures/sand_pebbles_rocks/sand_pebbles_rocks_Base_Color.png"
    property string rockTextureSource: ASSETS_PATH + "/textures/stone_mountain_grey/stone_mountain_grey_Base_Color.png"
    property string snowTextureSource: ASSETS_PATH + "/textures/icy_ground_snow/icy_ground_snow_Base_Color.png"
    property int maxTrianglesPerTexel: 10
    property int xDivisions: 100
    property int zDivisions: 100

    effect: TessellatedTerrainEffect {}

    parameters: [
        Parameter {
            name: "heightMap"
            value: TextureLoader {
                source: root.heightMapSource
            }
        },
        Parameter {
            name: "grassTexture"
            value: TextureLoader {
                source: root.grassTextureSource
            }
        },
        Parameter {
            name: "rockTexture"
            value: TextureLoader {
                source: root.rockTextureSource
            }
        },
        Parameter {
            name: "snowTexture"
            value: TextureLoader {
                source: root.snowTextureSource
            }
        },
        Parameter { name: "material.Ka"; value: root.ambient },
        Parameter { name: "material.Kd"; value: root.diffuse },
        Parameter { name: "material.Ks"; value: root.specular },
        Parameter { name: "material.shininess"; value: root.shininess },
        Parameter { name: "horizontalScale"; value: root.horizontalScale },
        Parameter { name: "verticalScale"; value: root.verticalScale },
        Parameter { name: "fog.color"; value: root.fogColor },
        Parameter { name: "fog.minDistance"; value: root.minFogDistance },
        Parameter { name: "fog.maxDistance"; value: root.maxFogDistance },
        Parameter { name: "maxTrianglesPerTexel"; value: root.maxTrianglesPerTexel },
        Parameter { name: "xDivisions"; value: root.xDivisions },
        Parameter { name: "zDivisions"; value: root.zDivisions }
    ]
}
