/* This file is part of Island Builder

   Copyright (C) 2014-2017 Sean Harmer <sh@theharmers.co.uk>
   Copyright (C) 2017 Kevin Ottens <ervin@ipsquad.net>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 3 of
   the License or any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.
*/

#include "tessellatedquadmesh.h"

#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QBuffer>
#include <Qt3DRender/QGeometryFactory>

#include <QtGui/QVector2D>

#include <QtCore/qvector.h>

namespace {
    template<class T>
    QByteArray createBufferData(const QVector<T> &values)
    {
        auto bufferData = QByteArray();
        bufferData.resize(values.size() * sizeof(T));

        auto rawData = reinterpret_cast<T *>(bufferData.data());
        memcpy(rawData, values.constData(), values.size() * sizeof(T));

        return bufferData;
    }
}

class TessellatedQuadMeshFactory : public Qt3DRender::QGeometryFactory
{
public:
    typedef QSharedPointer<TessellatedQuadMeshFactory> Ptr;
    QT3D_FUNCTOR(TessellatedQuadMeshFactory)

    TessellatedQuadMeshFactory(const TessellatedQuadMesh &other)
        : m_xResolution(other.xResolution())
        , m_zResolution(other.zResolution())
    {}

    bool operator ==(const Qt3DRender::QGeometryFactory &o) const override
    {
        const auto other = functor_cast<TessellatedQuadMeshFactory>(&o);
        return other
            && other->m_xResolution == m_xResolution
            && other->m_zResolution == m_zResolution;
    }

    Qt3DRender::QGeometry *operator ()() override
    {
        auto geometry = new Qt3DRender::QGeometry;

        // Each patch consists of a single point located at the lower-left corner
        // of a rectangle (in the xz-plane)
        const auto patchCount = m_xResolution * m_zResolution;
        auto positions = QVector<QVector2D>(patchCount);

        const auto dx = 1.0f / static_cast<float>(m_xResolution);
        const auto dz = 1.0f / static_cast<float>(m_zResolution);

        for (auto j = 0; j < m_zResolution; ++j) {
            const auto z = static_cast<float>(j) * dz;
            for (auto i = 0; i < m_xResolution; ++i) {
                const auto x = static_cast<float>(i) * dx;
                const auto index = m_xResolution * j + i;
                positions[index] = QVector2D(x, z);
            }
        }

        auto positionBuffer = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer, geometry);
        positionBuffer->setUsage(Qt3DRender::QBuffer::StaticDraw);
        positionBuffer->setData(createBufferData(positions));

        auto positionAttribute = new Qt3DRender::QAttribute(geometry);
        positionAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
        positionAttribute->setDataType(Qt3DRender::QAttribute::Float);
        positionAttribute->setDataSize(2);
        positionAttribute->setCount(positionBuffer->data().size() / sizeof(QVector2D));
        positionAttribute->setName(Qt3DRender::QAttribute::defaultPositionAttributeName());
        positionAttribute->setBuffer(positionBuffer);
        geometry->addAttribute(positionAttribute);

        return geometry;
    }

    const int m_xResolution;
    const int m_zResolution;
};


TessellatedQuadMesh::TessellatedQuadMesh(Qt3DCore::QNode *parent)
    : Qt3DRender::QGeometryRenderer(parent)
    , m_xResolution(10 * 1024 / 60)
    , m_zResolution(10 * 1024 / 60)
{
    setPrimitiveType(Patches);
    setVerticesPerPatch(1);
    installFactory();
}

void TessellatedQuadMesh::setXResolution(int xResolution)
{
    if (xResolution != m_xResolution) {
        m_xResolution = xResolution;
        installFactory();
        emit xResolutionChanged();
    }
}

int TessellatedQuadMesh::xResolution() const
{
    return m_xResolution;
}

void TessellatedQuadMesh::setZResolution(int zResolution)
{
    if (zResolution != m_zResolution) {
        m_zResolution = zResolution;
        installFactory();
        emit zResolutionChanged();
    }
}

int TessellatedQuadMesh::zResolution() const
{
    return m_zResolution;
}

void TessellatedQuadMesh::installFactory()
{
    setGeometryFactory(TessellatedQuadMeshFactory::Ptr::create(*this));
}
